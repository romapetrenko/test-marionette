var webpack = require('webpack');

module.exports = {
    entry: './app/index.coffee',
    externals: {
        'jquery': '$'
    },
    module: {
        loaders: [
            {
                test: /\.html$/,
                loader: 'underscore-template-loader'
            },
            {
                test: /\.coffee$/,
                loader: 'coffee-loader'
            },
        ]
    },
    output: {
        path: __dirname + '/static/js',
        filename: 'bundle.js'
    },
    plugins: [
        new webpack.ProvidePlugin({
            _: 'underscore'
        })
    ],
    resolve: {
        modulesDirectories: [__dirname + '/node_modules'],
        root: __dirname + '/app',
        extensions: ['', '.js', '.json', '.coffee']
    },
    resolveLoader: {
        root: __dirname + '/node_modules'
    }
};
