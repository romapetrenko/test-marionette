<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    public function testAlbums()
    {
        $client = static::createClient();
        $client->enableProfiler();
        $client->request('GET', '/albums');
        $response = $client->getResponse();
		$data = json_decode($response->getContent());
		
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertCount(5, $data);
    }
    
    public function testImagesAlbum1() {
    	$client = static::createClient();
    	$client->enableProfiler();
    	$client->request('GET', '/images/1');
    	$response = $client->getResponse();
    	$data = json_decode($response->getContent());
    	$this->assertEquals(1, $data->album->id);
    	$this->assertEquals('Album #1', $data->album->albumName);
    	$this->assertNotEmpty($data->paginationData);
    	$this->assertEquals(1, $data->paginationData->pageCount);
    	$this->assertEquals(5, $data->paginationData->totalCount);
    	$this->assertCount(5, $data->images);
    }
    
    public function testImagesAlbum2() {
    	$client = static::createClient();
    	$client->enableProfiler();
    	$client->request('GET', '/images/2');
    	$response = $client->getResponse();
    	$data = json_decode($response->getContent());
    	$this->assertEquals(2, $data->album->id);
    	$this->assertEquals('Album #2', $data->album->albumName);
    	$this->assertNotEmpty($data->paginationData);
    	$this->assertEquals(3, $data->paginationData->pageCount);
    	$this->assertEquals(25, $data->paginationData->totalCount);
    	$this->assertCount(10, $data->images);
    }
    
    public function testImagesAlbum2Page2() {
    	$client = static::createClient();
    	$client->enableProfiler();
    	$client->request('GET', '/images/2/2');
    	$response = $client->getResponse();
    	$data = json_decode($response->getContent());
    	$this->assertEquals(2, $data->album->id);
    	$this->assertEquals('Album #2', $data->album->albumName);
    	$this->assertNotEmpty($data->paginationData);
    	$this->assertEquals(3, $data->paginationData->pageCount);
    	$this->assertEquals(25, $data->paginationData->totalCount);
    	$this->assertEquals(2, $data->paginationData->current);
    	$this->assertCount(10, $data->images);
    	$this->assertEquals('image_2_11', $data->images[0]->imageName);
    }
    
    public function testImagesAlbum3() {
    	$client = static::createClient();
    	$client->enableProfiler();
    	$client->request('GET', '/images/3');
    	$response = $client->getResponse();
    	$data = json_decode($response->getContent());
    	$this->assertEquals(3, $data->album->id);
    	$this->assertEquals('Album #3', $data->album->albumName);
    	$this->assertNotEmpty($data->paginationData);
    	$this->assertEquals(3, $data->paginationData->pageCount);
    	$this->assertEquals(29, $data->paginationData->totalCount);
    	$this->assertCount(10, $data->images);
    	$this->assertEquals('image_3_1', $data->images[0]->imageName);
    }
    
    public function testImagesAlbum3Page2() {
    	$client = static::createClient();
    	$client->enableProfiler();
    	$client->request('GET', '/images/3/2');
    	$response = $client->getResponse();
    	$data = json_decode($response->getContent());
    	$this->assertEquals(3, $data->album->id);
    	$this->assertEquals('Album #3', $data->album->albumName);
    	$this->assertNotEmpty($data->paginationData);
    	$this->assertEquals(3, $data->paginationData->pageCount);
    	$this->assertEquals(29, $data->paginationData->totalCount);
    	$this->assertEquals(2, $data->paginationData->current);
    	$this->assertCount(10, $data->images);
    	$this->assertEquals('image_3_11', $data->images[0]->imageName);
    }
    
    public function testImagesAlbum4() {
    	$client = static::createClient();
    	$client->enableProfiler();
    	$client->request('GET', '/images/4');
    	$response = $client->getResponse();
    	$data = json_decode($response->getContent());
    	$this->assertEquals(4, $data->album->id);
    	$this->assertEquals('Album #4', $data->album->albumName);
    	$this->assertNotEmpty($data->paginationData);
    	$this->assertEquals(4, $data->paginationData->pageCount);
    	$this->assertEquals(33, $data->paginationData->totalCount);
    	$this->assertCount(10, $data->images);
    	$this->assertEquals('image_4_1', $data->images[0]->imageName);
    }
    
     public function testImagesAlbum4Page3() {
     	$client = static::createClient();
    	$client->enableProfiler();
    	$client->request('GET', '/images/4/3');
    	$response = $client->getResponse();
    	$data = json_decode($response->getContent());
    	$this->assertEquals(4, $data->album->id);
    	$this->assertEquals('Album #4', $data->album->albumName);
    	$this->assertNotEmpty($data->paginationData);
    	$this->assertEquals(4, $data->paginationData->pageCount);
    	$this->assertEquals(33, $data->paginationData->totalCount);
    	$this->assertEquals(3, $data->paginationData->current);
    	$this->assertCount(10, $data->images);
    	$this->assertEquals('image_4_21', $data->images[0]->imageName);
    }
}
