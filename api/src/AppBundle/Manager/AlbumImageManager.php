<?php

namespace AppBundle\Manager;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Knp\Component\Pager\Paginator;
use Doctrine\ORM\EntityManager;

use AppBundle\Entity\Image;
use AppBundle\Entity\Album;
use AppBundle\Entity\ImageRepository;
use AppBundle\Entity\AlbumRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use AppBundle\Transformer\ApiDataTransformer;

/**
 * Class to work with albums and images: query data, prepare data, transform data
 */
class AlbumImageManager implements ContainerAwareInterface {
	use ContainerAwareTrait;
	
	/**
	 * @var EntityManager
	 */
	private $em;
	
	/**
	 * @var ApiDataTransformer
	 */
	private $apiDataTranformer;
	
	/**
	 * @var AlbumRepository
	 */
	private $albumRepository;
	
	/**
	 * @var ImageRepository
	 */
	private $imageRepository;
	
	public function __construct(EntityManager $em, ApiDataTransformer $apiDataTranformer) {
		$this->em = $em;
		$this->apiDataTranformer = $apiDataTranformer;
		$this->imageRepository = $this->em->getRepository(Image::class);
		$this->albumRepository = $this->em->getRepository(Album::class);
	}
	
	/**
	 * Return all albums
	 */
	public function getAlbums($page = 1) {
		return $this->albumRepository->getAlbums($page);
	}
	
	public function getAlbumsNative() {
		$rsm = new ResultSetMapping();
		$rsm->addScalarResult('album_name', 'album_name');
		$rsm->addScalarResult('album_id', 'album_id');
		$rsm->addScalarResult('album_cover', 'album_cover');
		$rsm->addScalarResult('image_name', 'image_name');
		$rsm->addScalarResult('image_data', 'image_data');
		
		// used Native sql because task "one page with all albums, each album must contain maximum 10 images. They must receive by a single request to the database."
		$q = $this->em->createNativeQuery('SELECT a.album_name as album_name, a.cover_image as album_cover, a.id as album_id, i.id, i.image_name, i.image_data FROM albums a LEFT JOIN (SELECT i.* FROM images i LEFT JOIN images i2 ON i.album_id = i2.album_id AND i2.id < i.id GROUP BY i.id HAVING COUNT(*) < 10) i ON a.id = i.album_id', $rsm);
		$result = $q->getResult();
		return $this->apiDataTranformer->transformAlbums($result);
	}
	
	/**
	 * Return list of images for specified album and page
	 * @param integer $albumId
	 * @param integer $page
	 */
	public function getImages($albumId, $page = 1) {
		/**
		 * @var Album $album
		 */
		$album = $this->albumRepository->find($albumId);
		
		if ($album) {
			$paginator = $this->getKnpPaginator();
			$paginate = $paginator->paginate($this->imageRepository->getByAlbumQuery($album), $page, 10);
			$images = array();
			foreach ($paginate as $image) {
				$images[] = $image;
			}
			return array('album' => $album, 'images' => $images, 'paginationData' => $paginate->getPaginationData());
		}
		
		return array();
	}
	
	/**
	 * @return Paginator
	 */
	private function getKnpPaginator() {
		return $this->container->get('knp_paginator');
	}
}
