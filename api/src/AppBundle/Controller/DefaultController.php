<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Manager\AlbumImageManager;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * API to get album's list
     * @Route("/albums", name="api_albums")
     * @Method({"GET"})
     * @param Request $request
     */
    public function albumsAction(Request $request) {
//     	$r = $this->getAlbumImageManager()->getAlbumsNative();
//     	dump($r);
    	$page = $request->query->get('page', 1);
        //$response = new Response($this->getSerializer()->serialize($this->getAlbumImageManager()->getAlbumsNative(), 'json', array('groups' => array('api'))));
    	$response = new Response($this->getSerializer()->serialize($this->getAlbumImageManager()->getAlbumsNative(), 'json'));
        $response->headers->set('Content-Type', 'application/json');
        return $response;
    }
    
    /**
     * API to get images for particular album with pagination
     * @Route("/images/{albumId}/{page}", defaults={"page": 1}, name="api_images")
     * @param Request $request
     * @param integer $albumId
     * @param integer $page
     */
    public function imagesAction(Request $request, $albumId, $page) {
    	$response = new Response($this->getSerializer()->serialize($this->getAlbumImageManager()->getImages($albumId, $page), 'json', array('groups' => array('api'))));
    	$response->headers->set('Content-Type', 'application/json');
    	return $response;
    }
    
    /**
     * Service method to get AlbumImageManager instance
     * @return AlbumImageManager
     */
    private function getAlbumImageManager() {
    	return $this->get('album_image_manager');
    }
    
    /**
     * Service method to get Serializer instance
     * @return Serializer
     */
    private function getSerializer() {
    	return $this->get('serializer');
    }
}
