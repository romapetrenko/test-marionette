<?php

namespace AppBundle\Transformer;

use AppBundle\Model\ApiAlbum;
use AppBundle\Model\ApiImage;

class ApiDataTransformer
{
	public function transformAlbums($albumResult) {
		if (count($albumResult) > 0) {
			$data = array();
			$currentAlbum = -1;
			foreach($albumResult as $albumData) {
				if ($currentAlbum != $albumData['album_id']) {
					if ($currentAlbum != -1) {
						$data[] = $album;
					}
					$currentAlbum = $albumData['album_id'];
					$album = new ApiAlbum();
					$album->id = $albumData['album_id'];
					$album->albumName = $albumData['album_name'];
					$album->coverImage = $albumData['album_cover'];
				}
				$image = new ApiImage();
				$image->imageName = $albumData['image_name'];
				$image->imageData = $albumData['image_data'];
				$album->titleImages->add($image);
			}
			$data[] = $album;
			return $data;
		}
		
		return null;
	}
}