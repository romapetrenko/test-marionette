<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ImageRepository")
 * @ORM\Table(name="images")
 */
class Image {
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"api"})
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="image_name", type="string")
     * @Groups({"api"})
     * @var string
     */
    private $imageName;

    /**
     * @ORM\Column(name="image_data", type="text")
     * @Groups({"api"})
     * @var string
     */
    private $imageData;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Album", inversedBy="images")
     * @var \AppBundle\Entity\Album
     */
    private $album;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $imageName
     * @return Image
     */
    public function setImageName($imageName)
    {
        $this->imageName = $imageName;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getImageName()
    {
        return $this->imageName;
    }

    /**
     * Set imageData
     *
     * @param string $imageData
     * @return Image
     */
    public function setImageData($imageData)
    {
        $this->imageData = $imageData;

        return $this;
    }

    /**
     * Get imageData
     *
     * @return string
     */
    public function getImageData()
    {
        return $this->imageData;
    }

    /**
     * Set album
     *
     * @param \AppBundle\Entity\Album $album
     * @return Image
     */
    public function setAlbum(\AppBundle\Entity\Album $album = null)
    {
        $this->album = $album;

        return $this;
    }

    /**
     * Get album
     *
     * @return \AppBundle\Entity\Album
     */
    public function getAlbum()
    {
        return $this->album;
    }
}
