<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Entity\AlbumRepository")
 * @ORM\Table(name="albums")
 */
class Album {
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"api"})
     * @var integer
     */
    private $id;

    /**
     * @ORM\Column(name="album_name", type="string")
     * @Groups({"api"})
     * @var string
     */
    private $albumName;
    
    /**
     * @ORM\Column(name="cover_image", type="text")
     * @Groups({"api"})
     * @var string
     */
    private $coverImage;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Image", mappedBy="album", cascade={"persist", "remove"}, orphanRemoval=true)
     * @var \AppBundle\Entity\Image
     */
    private $images;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $albumName
     * @return Album
     */
    public function setAlbumName($albumName)
    {
        $this->albumName = $albumName;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getAlbumName()
    {
        return $this->albumName;
    }
    
    /**
     * Set cover image
     * 
     * @param string $coverImage
     * @return \AppBundle\Entity\Album
     */
    public function setCoverImage($coverImage) {
    	$this->coverImage = $coverImage;
    	
    	return $this;
    }
    
    /**
     * Get cover image
     * @return string
     */
    public function getCoverImage() {
    	return $this->coverImage;
    }

    /**
     * Add images
     *
     * @param \AppBundle\Entity\Image $images
     * @return Album
     */
    public function addImage(\AppBundle\Entity\Image $images)
    {
        $this->images[] = $images;

        return $this;
    }

    /**
     * Remove images
     *
     * @param \AppBundle\Entity\Image $images
     */
    public function removeImage(\AppBundle\Entity\Image $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }
}
