<?php

namespace AppBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

class ApiAlbum
{
	/**
	 * @Groups({"api"})
	 * @var integer
	 */
	public $id;
	
	/**
	 * @Groups({"api"})
	 * @var string
	 */
	public $albumName;
	
	/**
	 * @Groups({"api"})
	 * @var string
	 */
	public $coverImage;
	
	/**
	 * @Groups({"api"})
	 * @var ArrayCollection
	 */
	public $titleImages;
	
	public function __construct() {
		$this->titleImages = new ArrayCollection();
	}
}