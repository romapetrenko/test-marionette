<?php

namespace AppBundle\Model;
use Symfony\Component\Serializer\Annotation\Groups;

class ApiImage
{
	/**
	 * @Groups({"api"})
	 * @var string
	 */
	public $imageName;
	
	/**
	 * @Groups({"api"})
	 * @var string
	 */
	public $imageData;
}