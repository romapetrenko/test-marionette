Mn = require('backbone.marionette')
AlbumCollection = require('../collections/album')
LayoutView = require('../views/layout')

module.exports = class AppController extends Mn.Object
    initialize: ->
        console.log('controller init')
        this.options.regionManager = new Mn.RegionManager {
            regions: {
                main: '#img-gallery-app'
            }
        }

        albumCollection = new AlbumCollection()
        layout = new LayoutView {collection: albumCollection}
        this.getOption('regionManager').get('main').show(layout)
        this.options.layout = layout

    albumsList: ->

    albumImages: (id) ->
        console.log('controller albumImages with id' + id)
        layout = this.getOption 'layout'
        layout.triggerMethod 'show:album:images', id

    albumImagesPage: (id, page) ->
        console.log('controller AlbumImagesPage with id' + id + ' and page ' + page)
        layout = this.getOption 'layout'
        layout.triggerMethod 'show:album:images', id, page
