Backbone = require('backbone');

module.exports = class Image extends Backbone.Model
    defaults:
        id: 0,
        imageName: '',
        imageData: ''
