Backbone = require('backbone');

module.exports = class Album extends Backbone.Model
    defaults:
        albumName: ''
        id: 0
