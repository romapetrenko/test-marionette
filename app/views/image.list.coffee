Mn = require('backbone.marionette')

class ImageItemView extends Mn.ItemView
    template: require('../templates/images/list/item.html')
    tagName: 'div'
    className: 'col-sm-4 col-md-3 col-lg-3'

module.exports = class ImageListView extends Mn.CompositeView
    childView: ImageItemView
    childViewContainer: '#thumbnail-container'
    template: require('../templates/images/list.html')
    tagName: 'div'
    className: 'row'
