Mn = require('backbone.marionette')

class AlbumItemView extends Mn.ItemView
    template: require('../templates/album/list/item.html')
    tagName: 'div'
    className: 'col-lg-12 col-md-12 col-sm-12'

module.exports = class AlbumListView extends Mn.CompositeView
    childView: AlbumItemView
    childViewContainer: '#images-container'
    template: require('../templates/album/list.html')
    tagName: 'div'
    className: 'row'
