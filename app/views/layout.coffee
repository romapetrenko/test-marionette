Mn = require('backbone.marionette')
Backbone = require('backbone')

AlbumListView = require('../views/album.list')
AlbumCollection = require('../collections/album')
ImageListView = require('../views/image.list')
ImageCollection = require('../collections/image')
PaginationModel = require('../models/pagination')

module.exports = class LayoutView extends Mn.LayoutView
    template: require('../templates/layout.html')
    currentPage: 1
    albumCollection: new AlbumCollection();

    regions: {
        nav: '.nav'
        albumList: '.album-list',
        imageList: '.images-list'
    }

    ui: {
        showAlbums: '#show-albums'
    }

    events: {
        'click @ui.showAlbums': 'showAlbums'
    }

    onBeforeShow: ->
        console.log('onBeforeShow')
        this.loadAlbumsAndShow()

    onShowAlbumsList: ->

    showAlbums: ->
        # $('#show-albums').hide();
        # $('#images-container').show();

    loadAlbumsAndShow: ->
        $this = this
        this.albumCollection.fetch({ data: $.param({ page: this.currentPage }), remove: false }).done ->
            albumList = new AlbumListView {collection: $this.albumCollection}
            $this.showChildView('nav', albumList)

    onShowAlbumImages: (id, page = 1) ->
        console.log('onShowAlbumImages with album id=' + id + ' and page=' + page)
        imageCollection = new ImageCollection([], {albumId: id, page: page})
        $this = this
        imageCollection.fetch().done ->
            window.scrollTo(0, 0);
            # $('#show-albums').show();
            paginationModel = new PaginationModel { paginationData: imageCollection.paginationData, currentAlbum: imageCollection.currentAlbum }
            imageList = new ImageListView { collection: imageCollection, model: paginationModel }
            $this.showChildView('imageList', imageList)
            if page == 1
                Backbone.history.navigate('album/' + id);
            else
                Backbone.history.navigate('album/' + id + '/page/' + page);
