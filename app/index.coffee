Marionette = require('backbone.marionette')
Backbone = require('backbone');
Router = require('./routers/router')

class GalleryApp extends Marionette.Application
    onStart: (options) ->
        console.log('app start')
        router = new Router(options)
        Backbone.history.start()

galleryApp = new GalleryApp
galleryApp.start()
