Mn = require('backbone.marionette')
AppController = require('../controllers/app.controller')

module.exports = class Router extends Mn.AppRouter
    appRoutes: {
        '': 'albumsList'
        'album/:id': 'albumImages',
        'album/:id/page/:page': 'albumImagesPage'
    }

    initialize: ->
        console.log('router init')
        this.controller = new AppController
