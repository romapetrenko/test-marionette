Backbone = require('backbone')
ImageModel = require('../models/image')

module.exports = class ImageCollection extends Backbone.Collection
    model: ImageModel
    url: ->
        return appConfig.apiUrl + '/images/' + this.options.albumId + '/' + this.options.page
    initialize: (models, options) ->
        Backbone.Collection.prototype.initialize(this, models)
        this.options = options
    parse: (response) ->
        this.paginationData = response.paginationData
        this.currentAlbum = response.album
        return response.images
