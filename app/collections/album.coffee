Backbone = require('backbone');
AlbumModel = require('../models/album');

module.exports = class AlbumCollection extends Backbone.Collection
    model: AlbumModel
    url: ->
        return appConfig.apiUrl + "/albums"
