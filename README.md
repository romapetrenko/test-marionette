# README #

* Test job on symfony 2 and marionette
* Version 1
* demo: http://album-m.works.net4biz.biz/

```sh
git clone https://romapetrenko@bitbucket.org/romapetrenko/test-marionette.git
```
### Configure nginx ###

I splitted project to 2 domains: API and marionette. 
Document root for marionette is project folder, document root for API is project_folder/api/web/. It is not good idea on production, but it is only test project.
You can use album.nginx.conf from project directory. You need to change server_name and paths (may be listen port).

Also you need to change apiUrl in index.html

### Installation ###
```sh
cd test-marionette/
npm install
cd api/
composer install --ignore-platform-reqs
php app/console doctrine:database:create
php app/console doctrine:schema:update --force
php app/console doctrine:fixtures:load
```

### Build marionette project ###
You can not to do it, but if you want...
```sh
cd test-marionette/
npm install webpack --save-dev
node_modules/.bin/webpack
```

### Tests  ###
```sh
cd test-marionette/api/
phpunit -c app
```

### Contacts  ###

* Skype: roma.petrenko
* email: roma.petrenko@gmail.com